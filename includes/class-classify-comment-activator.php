<?php

/**
 * Fired during plugin activation
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Classify_Comment
 * @subpackage Classify_Comment/includes
 * @author     Alexandr Fedoryuk
 */
class Classify_Comment_Activator extends Classify_Comment_Ext
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		global $wpdb;

		require_once(ABSPATH . 'wp-admin/upgrade-functions.php');

		// Определение версии mysql
		// if ( version_compare(mysqli_get_server_info(), '4.1.0', '>=') ) {
		if (!empty($wpdb->charset))
			$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
		if (!empty($wpdb->collate))
			$charset_collate .= " COLLATE $wpdb->collate";
		// }

		// Структура нашей таблицы для отзывов
		$sql_table_classify_comments = "
		   CREATE TABLE `" . $wpdb->prefix . Classify_Comment_Ext::table_classify_comments . "` (
			   `ID` BIGINT UNSIGNED NULL AUTO_INCREMENT,
			   `comment_post_ID` BIGINT NOT NULL DEFAULT '0',
			   `comment_author` TINYTEXT,
			   `comment_author_email` VARCHAR(100),
			   `comment_author_url` VARCHAR(200),
			   `comment_author_IP` VARCHAR(100),
			   `comment_agent` VARCHAR(255),
			   `comment_content` TEXT,
			   `user_ID` BIGINT,
			   `cat` VARCHAR(20),
			   `date_filtered` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
			   PRIMARY KEY (`ID`),
			   INDEX (date_filtered DESC)
		   )" . $charset_collate . ";";
		$sql_table_classify_comments_options = "
		CREATE TABLE `" . $wpdb->prefix . Classify_Comment_Ext::table_classify_comments_option . "` (
			`name` VARCHAR(20) NOT NULL,
			`value` VARCHAR(200) NULL,
			PRIMARY KEY (`name`)
			)" . $charset_collate . ";";

		// Проверка на существование таблицы
		if ($wpdb->get_var("show tables like '" . $wpdb->prefix . Classify_Comment_Ext::table_classify_comments . "'") != $wpdb->prefix . Classify_Comment_Ext::table_classify_comments) {
			dbDelta($sql_table_classify_comments);
		}
		if ($wpdb->get_var("show tables like '" . $wpdb->prefix . Classify_Comment_Ext::table_classify_comments_option . "'") != $wpdb->prefix . Classify_Comment_Ext::table_classify_comments)
			dbDelta($sql_table_classify_comments_options);
	}
}
