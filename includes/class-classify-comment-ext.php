<?php

class Classify_Comment_Ext
{

    public const table_classify_comments = "classify_comments";

    public const table_classify_comments_option = "classify_comments_option";

    public $address;
    public $cats;

    public function __constructor()
    {
    }

    /**
     * Проверяет переданное сообщение и возвращает его категорию
     * @param string $msg - Сообщение
     * @return string - возвращает категорию
     */
    protected function verifyMessage($msg, $moreInfo = null)
    {
        $this->loadOptions();

        $data = array(
            'text' => $msg
        );
        $payload = json_encode($data);

        $ch = curl_init($this->address . "/api/v1/fisher");

        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        # Return response instead of printing.
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        # Send request.
        $result = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($result);

        global $wpdb;

        // error_log(var_dump($moreInfo) . "");

        $q = $wpdb->prepare(
            "
        INSERT INTO `" . $wpdb->prefix . Classify_Comment_Ext::table_classify_comments . "`
        (`comment_content`, `cat`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_agent`, `user_ID`)
        VALUES
        (%s, %s, %d, %s, %s, %s, %s, %s, %d)",
            $msg,
            $result->data,
            !is_null($moreInfo) ? $moreInfo['comment_post_ID'] : null,
            !is_null($moreInfo) ? $moreInfo['comment_author'] : null,
            !is_null($moreInfo) ? $moreInfo['comment_author_email'] : null,
            !is_null($moreInfo) ? $moreInfo['comment_author_url'] : null,
            !is_null($moreInfo) ? $moreInfo['comment_author_IP'] : null,
            !is_null($moreInfo) ? $moreInfo['comment_agent'] : null,
            !is_null($moreInfo) ? $moreInfo['user_ID'] : null
        );

        $wpdb->query($q);

        if ($result->result) return $result->data;



        return null;
    }

    /**
     * Проверка по списку блокировки категории
     * @param string Категория для проверки
     * @param boolean Если попадает в указанную категорию в настройках, то возвращает true
     */
    protected function validateCats($msg, $moreInfo = null)
    {
        $cat = $this->verifyMessage($msg, $moreInfo);

        // error_log($cat);

        if (count($this->cats) === 0) return false;

        foreach ($this->cats as $val) {
            if ($val == $cat) return true;
        }

        return false;
    }

    protected function loadOptions()
    {
        global $wpdb;

        $res = $wpdb->get_results("
		SELECT value FROM {$wpdb->prefix}classify_comments_option WHERE name = 'address'");
        $this->address = $res[0]->value;

        $res = $wpdb->get_results("
		SELECT value FROM {$wpdb->prefix}classify_comments_option WHERE name = 'cats'");

        $cats = array();

        if (isset($res) && count($res) > 0 && !is_null($res[0]->value)) {
            $res = explode(",", $res[0]->value);
            if (count($res) > 0) {
                foreach ($res as $val) {
                    $val = trim($val);
                    if (strlen($val) > 0) {
                        array_push($cats, $val);
                    }
                }
            }
        }

        $this->cats = array_unique($cats);
    }
}
