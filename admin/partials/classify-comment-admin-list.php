<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="classify-comments-content">
    <h1>Список обработанных комментариев</h1>
    <table>
        <tr>
            <th>Автор</th>
            <th>E-Mail</th>
            <th>URL</th>
            <th>IP</th>
            <th>Комментарий</th>
            <th>Агент</th>
            <th>Категория</th>
            <th>Дата фильтрации</th>
        </tr>
        <?php foreach ($res as $row) { ?>
            <tr>
                <td><?php echo $row->comment_author; ?></td>
                <td><?php echo $row->comment_author_email; ?></td>
                <td><?php echo $row->comment_author_url; ?></td>
                <td><?php echo $row->comment_author_IP; ?></td>
                <td><?php echo $row->comment_content; ?></td>
                <td><?php echo $row->comment_agent; ?></td>
                <td><?php echo $row->cat; ?></td>
                <td><?php echo $row->date_filtered; ?></td>
            </tr>
        <?php } ?>
    </table>

    <p>Всего показано строк: <?php echo count($res); ?>

</div>