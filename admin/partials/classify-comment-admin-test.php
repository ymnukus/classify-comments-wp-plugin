<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/admin/partials
 */
?>

<form method="POST">
    <p>
        <label>Введите сообщение для проверки</label>
        <input type="text" name="message" value="<?php echo $message; ?>">
    </p>
    <input type="submit" value="Проверить">
</form>

<?php if (isset($result) && !is_null($result)) { ?>

    <div class="result">
        <p>
            Результат: <?php echo $result; ?>
        </p>
    </div>

<?php } ?>