<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="classify-comments-content">
    <h1>Настройки Токсичности</h1>
    <form method="POST">
        <p>
            <label>Адрес обработчика</label>
            <input name="address" value="<?php echo $options['address']; ?>">
        </p>
        <p>
            <label>Категории для блокировки (через запятую)</label>
            <input name="cats" value="<?php echo implode(", ", $options['cats']); ?>">
        </p>
        <input type="submit" value="Сохранить">
    </form>
</div>