<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/admin
 * @author     Alexandr Fedoryuk
 */
class Classify_Comment_Admin extends Classify_Comment_Ext
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $classify_comment    The ID of this plugin.
	 */
	private $classify_comment;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $classify_comment       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($classify_comment, $version)
	{

		$this->classify_comment = $classify_comment;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{
		wp_enqueue_style($this->classify_comment, plugin_dir_url(__FILE__) . 'css/classify-comment-admin.css', array(), $this->version, 'all');
	}

	/**
	 * Генерируем меню
	 */
	public function admin_generate_menu()
	{
		// Добавляем основной раздел меню
		add_menu_page('Добро пожаловать в модуль классификации комментариев', 'Токсичность', 'manage_options', 'options_toxic_comments', array(&$this, 'admin_options'));;

		// Добавляем дополнительный раздел
		add_submenu_page('options_toxic_comments', 'Управление содержимом', 'Проверка', 'manage_options', 'toxic_test', array(&$this, 'admin_comment_test'));
		add_submenu_page('options_toxic_comments', 'Управление содержимом', 'Список сообщений', 'manage_options', 'toxic_list', array(&$this, 'admin_comment_list'));
	}

	/**
	 * Настройка параметров плагина
	 */
	public function admin_options()
	{
		global $wpdb;

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			// Обновляем данные настроек
			$q = $wpdb->prepare("
			INSERT INTO {$wpdb->prefix}classify_comments_option (name, value)
			VALUES ('address' ,%s)
			ON DUPLICATE KEY UPDATE value = %s", $_POST['address'], $_POST['address']);
			$wpdb->query($q);

			$q = $wpdb->prepare("
			INSERT INTO {$wpdb->prefix}classify_comments_option (name, value)
			VALUES ('cats'
			, %s)
			ON DUPLICATE KEY UPDATE value = %s", $_POST['cats'], $_POST['cats']);
			$wpdb->query($q);
		}

		$this->loadOptions();

		$options = array(
			'address' => $this->address,
			'cats' => $this->cats
		);

		require_once('partials/classify-comment-admin-options.php');
	}

	/**
	 * Список проверенных сообщений
	 */
	public function admin_comment_list()
	{
		global $wpdb;

		$res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}classify_comments ORDER BY date_filtered DESC LIMIT 500");

		require_once('partials/classify-comment-admin-list.php');
		// TODO
	}

	/**
	 * Проверка работы сервиса
	 */
	public function admin_comment_test()
	{

		$result = null;

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$result = $this->verifyMessage($_POST['message']);
		}

		require_once('partials/classify-comment-admin-test.php');
	}
}
