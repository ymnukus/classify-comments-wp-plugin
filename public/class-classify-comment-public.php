<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Classify_Comment
 * @subpackage Classify_Comment/public
 * @author     Alexandr Fedoryuk
 */
class Classify_Comment_Public extends Classify_Comment_Ext
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $classify_comment    The ID of this plugin.
	 */
	private $classify_comment;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $classify_comment       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($classify_comment, $version)
	{

		$this->classify_comment = $classify_comment;
		$this->version = $version;
	}

	/**
	 * Проверяем комментарий на токсичность. Если это будет группа из списка настроек, то вернем результат 'spam' чтобы заблокировать.
	 */
	public function pre_filter_comment($approved, $commentdata)
	{
		// error_log(var_dump($commentdata) . "");

		if ($approved === 'spam') return $approved;

		if ($this->validateCats($commentdata['comment_content'], $commentdata) === true) return 'spam';

		return $approved;
	}
}
