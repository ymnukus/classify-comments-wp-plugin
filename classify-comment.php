<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Classify_Comment
 *
 * @wordpress-plugin
 * Plugin Name:       Classify Comment
 * Plugin URI:        http://ymnuktech.ru
 * Description:       Classify comments for toxic messages
 * Version:           1.0.0
 * Author:            Alexandr Fedoryuk
 * Author URI:        http://ymnuktech.ru
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       classify-comment
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Update it as you release new versions.
 */
define( 'CLASSIFY_COMMENT_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-classify-comment-activator.php
 */
function activate_classify_comment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-classify-comment-activator.php';
	Classify_Comment_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-classify-comment-deactivator.php
 */
function deactivate_classify_comment() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-classify-comment-deactivator.php';
	Classify_Comment_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_classify_comment' );
register_deactivation_hook( __FILE__, 'deactivate_classify_comment' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-classify-comment.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_classify_comment() {

	$plugin = new Classify_Comment();
	$plugin->run();

}
run_classify_comment();
